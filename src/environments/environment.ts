// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firebase: {
		apiKey: 'AIzaSyDrX8qKo7-sKb74OQ1cKAhAK_Ysy0j0qm4',
		authDomain: 'skolka-brest.firebaseapp.com',
		databaseURL: 'https://skolka-brest.firebaseio.com',
		projectId: 'skolka-brest',
		storageBucket: 'skolka-brest.appspot.com',
		messagingSenderId: '179839056575',
		appId: '1:179839056575:web:d56e596abecf74d62a240a',
		measurementId: 'G-TZPWJCKBRF'
	}
	/*firebase: {
		apiKey: 'AIzaSyCztUDVls0JizutTCVNmodIe5dRbMlrbmo',
		authDomain: 'testing-skolka-brest.firebaseapp.com',
		projectId: 'testing-skolka-brest',
		storageBucket: 'testing-skolka-brest.appspot.com',
		messagingSenderId: '26239160050',
		appId: '1:26239160050:web:b97191730d5d8192740a6b',
		measurementId: 'G-3X59LLYWKJ'
	}*/,
	recaptcha: '6LeLVm4eAAAAADkcSBkV5PObWQ6kRR09u_RRBEP1'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
