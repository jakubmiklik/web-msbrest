import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from './services/authorization.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
	title = 'skolka';
	favIcon: HTMLLinkElement = document.querySelector('#favIcon');

	ngOnInit(): void {

		window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
			? this.favIcon.href = './assets/favicon-dark.ico'
			: this.favIcon.href = './assets/favicon-light.ico';
	}
}

