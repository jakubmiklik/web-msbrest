import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FireStorageService } from 'src/app/services/firestorage.service';
import {getDownloadURL, getMetadata} from '@angular/fire/storage';
import {AuthorizationService} from '../../services/authorization.service';

@Component({
	selector: 'app-documents',
	templateUrl: './documents.component.html',
	styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

	folders;
	documents = [];
	path;

	constructor(
		public fireStorageService: FireStorageService,
		public activatedRoute: ActivatedRoute,
		public authService: AuthorizationService) { }

	async ngOnInit(): Promise<void> {
		this.activatedRoute.paramMap.subscribe((params) => {
			params.get('category') ? this.path = decodeURI(params.get('category')) : this.path = 'Documents';
			this.fireStorageService.getLists(this.path).then(res => {
				if (res.prefixes){
					this.folders = res.prefixes;
				}
				if (res.items){
					this.documents = [];
					res.items.forEach(async (file) => {
						const src = await getDownloadURL(file);
						const metadata = (await getMetadata(file)).customMetadata;
						let isPrivate = false;
						if(metadata) {
							isPrivate = metadata.private === 'false' ? false : isPrivate;
							isPrivate = metadata.private === 'true' ?? true;
						}
						if(this.authService.loggedIn || !this.authService.loggedIn && !isPrivate) {
							this.documents.push({name: file.name, src, path: file.fullPath, fileRef: file});
						}
					});
				}
			});
		});
	}



}
