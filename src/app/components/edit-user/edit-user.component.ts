import { Component, OnInit } from '@angular/core';
import {AuthorizationService} from '../../services/authorization.service';
import {User} from '@angular/fire/auth';

@Component({
	selector: 'edit-user',
	templateUrl: './edit-user.component.html',
	styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

	private user: User;
	private _username = '';
	get userName(): string {
		return this._username;
	}
	set userName(text) {
		this._username = text;
	}

	constructor(private authService: AuthorizationService) { }

	ngOnInit(): void {
		this.user = this.authService.getUser();
		this.userName = this.user.displayName;

	}

	updateProfile(): void {
		this.authService.updateUser(this.userName);
	}

}
