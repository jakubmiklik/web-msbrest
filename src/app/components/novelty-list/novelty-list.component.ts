import { Component, OnInit } from '@angular/core';
import {Novelty} from '../../interfaces';
import {NewsService} from '../../services/news.service';

@Component({
	selector: 'app-novelty-list',
	templateUrl: './novelty-list.component.html',
	styleUrls: ['./novelty-list.component.scss']
})
export class NoveltyListComponent implements OnInit {

	news: Novelty[];

	constructor(private newsService: NewsService) {
	}

	async ngOnInit(): Promise<void> {
		this.news = await this.newsService.getNews();
		this.news.forEach((novelty) => novelty.text = novelty.text.replace(/<[^>]*>?/gm, ''));
	}
}

