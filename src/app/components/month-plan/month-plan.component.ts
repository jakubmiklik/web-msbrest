import { Component, OnInit } from '@angular/core';
import {FirestoreService} from '../../services/firestore.service';
import {MonthPlan} from '../../interfaces';

@Component({
	selector: 'month-plan',
	templateUrl: './month-plan.component.html',
	styleUrls: ['./month-plan.component.scss'],
})

export class MonthPlanComponent implements OnInit {

	monthPlan: MonthPlan[] = [];

	constructor(private firestoreService: FirestoreService) {}

	ngOnInit() {
		this.firestoreService.getDocuments('month-plan').then((querySnap) => {
			this.monthPlan = querySnap.docs
				.map((monthPlan) => monthPlan.data() as MonthPlan)
				.sort((a, b) => {
					if (a.year < b.year) { return 1; }
					if (a.year > b.year) { return -1; }
					if (a.month < b.month) { return 1; }
					if (a.month > b.month) { return -1; }
				});;
		});
	}
}

