import { Component, HostListener, Input, OnInit } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { AuthorizationService } from '../../services/authorization.service';
import { Subscription } from 'rxjs';
import {Event, Router} from '@angular/router';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

	get device(): boolean {
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || window.innerWidth < 1200;
	}

	show = false;
	showUserMenu = false;
	@Input() displayName: string;

	touchstartX = 0;
	touchendX = 0;

	items: IMenu[] = [
		{
			name: 'Domů',
			link: '/',
			icon: 'home',
		},
		{
			name: 'Aktuality',
			link: '/aktuality',
			icon: 'newspaper',
		},
		{
			name: 'O školce',
			icon: 'people',
			submenu: [
				{
					name: 'Charakteristika a Fylozofie',
					link: '/o-skolce',
					icon: 'info',
				},
				{
					name: 'Přijímací řízení',
					link: '/prijimaci-rizeni',
					icon: 'assignment',
				},
				{
					name: 'Školní vzdělávací program',
					link: '/svp',
					icon: 'integration_instructions',
				}
			]
		},
		{
			name: 'Dokumenty',
			link: '/dokumenty',
			icon: 'folder',
		},
		{
			name: 'Fotogalerie',
			link: '/fotogalerie',
			icon: 'photo_camera',
		},
		{
			name: 'Kontakty',
			link: '/kontakty',
			icon: 'contacts',
		}
	]
	userItems: IMenu[] = [{
		name: 'Můj profil',
		link: '/muj-profil',
		icon: 'person',
		submenu: [
			{
				name: 'Přidat absenci',
				link: '/pridat-absenci',
				icon: 'add',
			},
			{
				name: 'Absence',
				link: '/absence',
				icon: 'list',
			}
		]
	}];


	constructor(public authService: AuthorizationService, public router: Router) { }
	ngOnInit() {
		const gestureZone = document.querySelector('body');
		gestureZone.addEventListener('touchstart', (event) => {
			this.touchstartX = event.changedTouches[0].screenX;
		}, false);

		gestureZone.addEventListener('touchend', (event) => {
			this.touchendX = event.changedTouches[0].screenX;
			if(this.touchstartX > (window.innerWidth * 0.9)) {
				this.handleGesture();
			}
		}, false);
	}

	public handleGesture(): void {
		if (this.touchendX < this.touchstartX) {
			if(!this.show) {
				this.show = document.querySelector('nav').classList.toggle('show-menu');
			} else {
				if(this.authService.loggedIn)
				this.show = document.querySelector('nav').classList.toggle('show-menu');
				this.showUserMenu = document.querySelector('.user-wrapper .sub-menu').classList.toggle('show-menu');
			}
		}

		if (this.touchendX > this.touchstartX) {
			if(this.show) {
				this.show = document.querySelector('nav').classList.toggle('show-menu');
			}
			if(this.showUserMenu) {
				this.showUserMenu = document.querySelector('.user-wrapper .sub-menu').classList.toggle('show-menu');
			}
		}
	}

	public toggle(event?: MouseEvent, isLink: boolean = true, userButton: boolean = false, navButton: boolean = false): void {
		if(this.device) {
			if(event && !isLink) {
				this.toggleSubMenu(event);
				return;
			}
			if(userButton) {
				if(this.show) {
					this.show = document.querySelector('nav').classList.toggle('show-menu');
				}
				this.showUserMenu = document.querySelector('.user-wrapper .sub-menu').classList.toggle('show-menu');
			}
			if(navButton) {
				if(this.showUserMenu) {
					this.showUserMenu = document.querySelector('.user-wrapper .sub-menu').classList.toggle('show-menu');
				}
				this.show = document.querySelector('nav').classList.toggle('show-menu');
			}

			if(isLink) {
				if(this.show) {
					this.show = document.querySelector('nav').classList.toggle('show-menu');
				}
				if(this.showUserMenu) {
					this.showUserMenu = document.querySelector('.user-wrapper .sub-menu').classList.toggle('show-menu');
				}
			}

			this.show || this.showUserMenu
				? document.querySelector('body').style.overflow = 'hidden'
				: document.querySelector('body').style.overflow = 'auto';
		}
	}

	public toggleSubMenu(event: MouseEvent): void {
		event.stopPropagation();
		event.preventDefault();
		const subMenu = (event.target as Element).closest('.link').parentElement.querySelector('.sub-menu');
		const showed = subMenu.classList.toggle('show');
		setTimeout(() => {
			subMenu.setAttribute('style', `height: ${showed ? 'fit-content !important' : 0}`);
		}, showed ? 0 : 350);
	}
}

interface IMenu {
	name: string;
	link?: string;
	icon: string;
	submenu?: IMenu[];
}
