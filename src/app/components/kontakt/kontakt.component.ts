import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-kontakt',
  templateUrl: './kontakt.component.html',
  styleUrls: ['./kontakt.component.scss']
})
export class KontaktComponent implements OnInit {

  constructor() { }
  
  data = [
    {title: "Název školy", data: "Mateřská škola Břest 227, okres Kroměříž"},
    {title: "Adresa", data: "MŠ Břest 227, 76823 Břest"},
    {title: "Telefon",data: "573 354 032"},
    {title: "Mobil",data: "+420 734 617 554"},
    {title: "Email",data: "msbrest@seznam.cz"},
    {title: "Právní forma",data: "Příspěvková organizace"},
    {title: "IČ",data: "70991294"},
    {title: "Datová schránka",data: "isy3e5v"},
    {title: "Zařazení do sítě škol",data: "1. 1. 2003"},
    {title: "Název zřizovatele",data: "Obec Břest"},
    {title: "IZO",data: "600 118 045"},
    {title: "Ředitelka",data: "Bc. Miroslava Brabcová"},
    {title: "Počet tříd",data: "2"},
    {title: "Provoz MŠ",data: "zajištěn od 6:30 do 16:00 hodin"}];
  ngOnInit() {
      
  }

}
