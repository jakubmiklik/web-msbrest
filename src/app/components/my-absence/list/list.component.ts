import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { FirestoreService} from 'src/app/services/firestore.service';
import { Absence, Child } from 'src/app/interfaces';
import { map, switchMap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

	userUID = '';
	children: Child[] = [];
	currentTab = 0;
	device = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	constructor(
		public firestoreService: FirestoreService,
		public authService: AuthorizationService,
		public router: Router,
		public cookie: CookieService)
	{	}


	ngOnInit(): void {
		this.userUID = this.authService.getUser().uid;
		this.firestoreService.getDocuments(`users/${this.userUID}/children`).then((children) => {
			children.docs.map((child) => child.data() as Child).forEach((child) => {
				this.firestoreService.getDocuments(`users/${this.userUID}/children/${child.id}/absence`).then((absence) => {
					child.absence = absence.docs.map((abs) => abs.data() as Absence)
						.sort((a,b) => b.dateTo - a.dateTo);
					this.children.push(child);
				});
			});
		});
	}

	getSelectedOption(event: Event): number {
		const element = event.target as HTMLInputElement;
		console.log(element.value);
		return +element.value;
	}
}
