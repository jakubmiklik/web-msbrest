import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { FirestoreService} from 'src/app/services/firestore.service';
import { Absence, Child, CheckedChildren } from 'src/app/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
	selector: 'app-add-absence',
	templateUrl: './add-absence.component.html',
	styleUrls: ['./add-absence.component.scss']
})
export class AddAbsenceComponent implements OnInit {
	userUID: string;
	children: Child[] = [];
	arrayOfChoosenChildren: CheckedChildren[] = [];
	allCheck = false;
	absence: Absence = {
		dateFrom: 0,
		dateTo: 0,
		text: '',
		childName: ''
	};

	public allChecks() {
		this.allCheck = this.arrayOfChoosenChildren.every((child) => child.checked);
	}

	public toggleChecks() {
		this.arrayOfChoosenChildren.forEach((child) => child.checked = this.allCheck);
	}

	constructor(
		public fireService: FirestoreService,
		public authService: AuthorizationService,
		public router: Router,
		public snackBar: MatSnackBar) { }

	ngOnInit(): void {
		this.userUID = this.authService.getUser().uid;
		this.fireService.getDocuments(`users/${this.authService.getUser().uid}/children`).then((query) => {
			this.children = query.docs.map((children) => children.data() as Child);
			this.children.forEach(() => this.arrayOfChoosenChildren.push({checked: false}));
		});
	}

	getDate(event: Event) {
		const ev = event.target as HTMLInputElement;
		return ev.valueAsNumber;
	}

	getValue(event: Event) {
		const ev = event.target as HTMLInputElement;
		return ev.value;
	}

	postData() {
		for (let i = 0; i < this.arrayOfChoosenChildren.length; i++) {
			if (this.arrayOfChoosenChildren[i].checked) {
				this.absence.childName = this.children[i].name;
				this.fireService.setDocument(
					`users/${this.userUID}/children/${this.children[i].id}/absence`,
					this.absence,
					false);
			}
		}
	}

	onSubmit() {
		this.postData();
		this.snackBar.open('Absence přidána', null, {
			duration: 5000,
			horizontalPosition: 'end',
			verticalPosition: 'bottom'
		});
		this.router.navigate(['/absence']);
	}

}
