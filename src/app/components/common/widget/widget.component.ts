import {Component, Input, OnInit} from '@angular/core';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';
import Timer = NodeJS.Timer;

@Component({
	selector: 'widget',
	templateUrl: './widget.component.html',
	styleUrls: ['./widget.component.scss'],
	animations: [
		trigger('carouselAnimation', [
			transition(':increment', [
				query(':enter', [
					style({ opacity: 0 }),
					stagger(0, [
						animate('300ms ease-out', style({ opacity: 1 })),
					]),
				], { optional: true })
			]),
			transition('* => 0', [
				query(':enter', [
					style({ opacity: 0 }),
					stagger(0, [
						animate('300ms ease-out', style({ opacity: 1 })),
					]),
				], { optional: true })
			]),
		])
	]
})
export class WidgetComponent implements OnInit {
	private isPaused = true;
	private interval: Timer;
	
	private _currentSlide: number = 0;
	
	get currentSlide(): number {
		return this._currentSlide;
	}
	set currentSlide(slide :number) {
		this._currentSlide = slide;
	}
	
	@Input() indicatorEnabled: boolean = true;
	@Input() title: string;
	@Input("data") widgets: [];

	constructor() { }

	ngOnInit(): void { }
	public selectSlide(index?: number) {
		if(index !== undefined) {
			this.currentSlide = index;
			return;
		}
		
		this.currentSlide = index + 1 === this.widgets.length ? index : index;
		if (this.isPaused && index !== undefined) {
			clearInterval(this.interval);
		} else {
			clearInterval(this.interval);
		}
	}
}
