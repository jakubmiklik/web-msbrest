import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthorizationService} from '../../services/authorization.service';

@Component({
	selector: 'app-login',
	templateUrl: './login-page.component.html',
	styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

	loginForm = new FormGroup({
		email: new FormControl(),
		password: new FormControl()
	});

	constructor(public authService: AuthorizationService) {
	}

	ngOnInit() {
	}

	signIn(): void {
		this.authService.signIn(this.loginForm.get('email').value, this.loginForm.get('password').value);
	}

}
