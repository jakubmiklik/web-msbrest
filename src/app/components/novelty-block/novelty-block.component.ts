import { Component, OnInit } from '@angular/core';
import {NewsService} from '../../services/news.service';
import {Novelty} from '../../interfaces';

@Component({
	selector: 'novelty-block',
	templateUrl: './novelty-block.component.html',
	styleUrls: ['./novelty-block.component.scss'],
})
export class NoveltyBlockComponent implements OnInit {

	news : Novelty[] = [];

	constructor(public newsService: NewsService) {

	}

	async ngOnInit(): Promise<void> {
		this.news = await this.newsService.getNews();
		this.news.forEach((novelty) => novelty.text = novelty.text.replace(/<[^>]*>?/gm, ''));
	}
}
