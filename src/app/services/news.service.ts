import { Injectable } from '@angular/core';
import {Novelty} from '../interfaces';
import {FirestoreService} from './firestore.service';

@Injectable({
	providedIn: 'root'
})
export class NewsService {

	constructor(private firestoreService: FirestoreService) { }

	async getNews(): Promise<Novelty[]> {
		let news: Novelty[] = [];
		return this.firestoreService.getDocuments('articles').then((articles) => {
			news = articles.docs
				.map((article) => article.data() as Novelty)
				.sort((a, b) => (b.date as number) - (a.date as number));
			news.forEach((novelty) => {
				novelty.date = new Date(novelty.date).toLocaleString();
			});
			news.forEach(novelty => {
				if (novelty.files) {
					novelty.documents = novelty.files.filter((file) => {
						return file.type.includes('document') || file.type.includes('pdf');
					});
					novelty.images = novelty.files.filter((file) => {
						return file.type.includes('image');
					});
				}
			});
			return news;
		});
	}
}
