import {Injectable, Optional} from '@angular/core';
import {NavigationEnd, Router, RoutesRecognized} from '@angular/router';
import {Auth, authState, getAuth, onAuthStateChanged, signInWithEmailAndPassword, signOut, updateProfile, User} from '@angular/fire/auth';
import {EMPTY, filter, Observable, pairwise, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {traceUntilFirst} from '@angular/fire/performance';

@Injectable({
	providedIn: 'root'
})

export class AuthorizationService {

	private readonly userDisposable: Subscription|undefined;
	public readonly user: Observable<User | null> = EMPTY;
	private _loggedIn = false;
	public previousUrl = '';
	get loggedIn(): boolean {
		return this._loggedIn;
	}
	set loggedIn(state) {
		this._loggedIn = state;
	}
	constructor(
		public router: Router,
		@Optional() private auth: Auth,
	) {
		if (auth) {
			this.user = authState(this.auth);
			this.userDisposable = authState(this.auth).pipe(
				traceUntilFirst('auth'),
				map(u => {
					return !!u;
				})
			).subscribe((isLoggedIn) => {
				this.loggedIn = isLoggedIn;
			});
		}
		this.router.events
			.pipe(filter((event: any) => event instanceof RoutesRecognized), pairwise())
			.subscribe((event: RoutesRecognized[]) => {
				this.previousUrl = event[0].urlAfterRedirects;
			});
	}

	public signIn(login: string, password: string): void {

		signInWithEmailAndPassword(getAuth(), login, password)
			.then((userC) => {
				this.loggedIn = !!userC.user;
				this.router.navigate([this.previousUrl]);
			})
			.catch(() => alert('Neplatné přihlašovací jméno nebo heslo, prosím, zkuste to znovu.'));
	}

	logOut() {
		signOut(getAuth()).then(() => this.loggedIn = !!getAuth().currentUser);
		this.router.navigate(['']);
	}

	getUser(){
		return getAuth().currentUser;
	}
	updateUser(displayName: string): void {
		updateProfile(getAuth().currentUser, {displayName});
	}
}
