import { Injectable } from '@angular/core';
import {
	doc, getDoc, getFirestore, DocumentData,
	collection, query, where, getDocs,
	QuerySnapshot, FieldPath, WhereFilterOp, setDoc
} from '@angular/fire/firestore';

@Injectable({
	providedIn: 'root'
})
export class FirestoreService {

	private db = getFirestore();

	constructor() {}

	async getDocument(path: string): Promise<DocumentData> {
		const docRef = doc(this.db, path);
		const docSnap = await getDoc(docRef);
		return docSnap.data();
	}

	async getDocuments(path: string): Promise<QuerySnapshot<DocumentData>> {
		const q = query(collection(this.db, path));
		const querySnapshot = await getDocs(q);
		return querySnapshot;
	}

	async getDocumentsWhere(path: string,
		whereData: {left: string | FieldPath, operator: WhereFilterOp, right: any}): Promise<QuerySnapshot<DocumentData>> {

		const q = query(collection(this.db, path), where(whereData.left, whereData.operator, whereData.right));
		const querySnapshot = await getDocs(q);
		return querySnapshot;
	}

	setDocument(path: string, data: any, merge: boolean){
		return setDoc(doc(collection(this.db, path)), data, {merge});
	}
}
