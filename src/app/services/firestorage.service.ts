import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import {getStorage, ref, listAll, ListResult} from '@angular/fire/storage';

@Injectable({
	providedIn: 'root'
})

export class FireStorageService {
	public storage = getStorage();
	constructor(
		public snackBar: MatSnackBar,
		public route: Router
	) { }

	async getLists(path: string): Promise<ListResult> {
		return await listAll(ref(this.storage, path));
	}
}
