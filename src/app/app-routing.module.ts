import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdmissionsComponent } from './components/admissions/admissions.component';
import { CharacteristicsComponent } from './components/characteristics/characteristics.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { KontaktComponent } from './components/kontakt/kontakt.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { AddAbsenceComponent } from './components/my-absence/add-absence/add-absence.component';
import { ListComponent } from './components/my-absence/list/list.component';
import { NoveltyDetailComponent } from './components/novelty-detail/novelty-detail.component';
import { NoveltyListComponent } from './components/novelty-list/novelty-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PhotogalleryComponent } from './components/photogallery/photogallery.component';
import { SecureInnerPagesGuard } from './guards/secure-inner-pages.guard';
import { SvpComponent } from './components/svp/svp.component';
import {EditUserComponent} from './components/edit-user/edit-user.component';


const routes: Routes = [
  {path: '', pathMatch: 'full', component: HomepageComponent},
  {path: 'kontakty', component: KontaktComponent},
  {path: 'fotogalerie', component: PhotogalleryComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'fotogalerie/:category', component: PhotogalleryComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'aktuality', component: NoveltyListComponent},
  {path: 'aktuality/:id', component: NoveltyDetailComponent},
  {path: 'o-skolce', component: CharacteristicsComponent},
  {path: 'svp', component: SvpComponent},
  {path: 'prijimaci-rizeni', component: AdmissionsComponent},
  {path: 'dokumenty', component: DocumentsComponent},
  {path: 'dokumenty/:category', component: DocumentsComponent},
  {path: 'prihlaseni', component: LoginPageComponent},
  {path: 'absence', component: ListComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'pridat-absenci', component: AddAbsenceComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'muj-profil', component: EditUserComponent, canActivate: [SecureInnerPagesGuard]},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
