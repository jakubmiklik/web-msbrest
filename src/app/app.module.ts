import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {getFirestore, provideFirestore} from '@angular/fire/firestore';
import {getStorage, provideStorage} from '@angular/fire/storage';
import {getAuth, provideAuth} from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { AuthorizationService } from './services/authorization.service';
import { FirestoreService } from './services/firestore.service';
import { KontaktComponent } from './components/kontakt/kontakt.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { HeaderComponent } from './components/header/header.component';
import { MonthPlanComponent } from './components/month-plan/month-plan.component';
import { CharacteristicsComponent } from './components/characteristics/characteristics.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { PhotogalleryComponent } from './components/photogallery/photogallery.component';
import { NoveltyBlockComponent } from './components/novelty-block/novelty-block.component';
import { NoveltyDetailComponent } from './components/novelty-detail/novelty-detail.component';
import { NoveltyListComponent } from './components/novelty-list/novelty-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { AdmissionsComponent } from './components/admissions/admissions.component';
import { ListComponent } from './components/my-absence/list/list.component';
import { AddAbsenceComponent } from './components/my-absence/add-absence/add-absence.component';
import { SvpComponent } from './components/svp/svp.component';
import { DocumentsComponent } from './components/documents/documents.component';
import { FireStorageService } from './services/firestorage.service';
import { initializeApp, provideFirebaseApp} from '@angular/fire/app';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import {NewsService} from './services/news.service';
import { WidgetComponent } from './components/common/widget/widget.component';

@NgModule({
	imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		MatSnackBarModule,
		BrowserAnimationsModule,
		provideFirebaseApp(() => initializeApp(environment.firebase)),
		/*provideAppCheck(() => initializeAppCheck(getApp(), {
			provider: new ReCaptchaV3Provider(environment.recaptcha),
		})),*/
		provideFirestore(() => getFirestore()),
		provideStorage(() => getStorage()),
		provideAuth(() => getAuth()),
		ReactiveFormsModule,
	],
	declarations: [
		AppComponent,
		KontaktComponent,
		HomepageComponent,
		HeaderComponent,
		NoveltyBlockComponent,
		NoveltyDetailComponent,
		NoveltyListComponent,
		MonthPlanComponent,
		CharacteristicsComponent,
		LoginPageComponent,
		PhotogalleryComponent,
		PageNotFoundComponent,
		FooterComponent,
		AdmissionsComponent,
		ListComponent,
		AddAbsenceComponent,
		SvpComponent,
		DocumentsComponent,
  		EditUserComponent,
    WidgetComponent
	],
	providers: [AuthorizationService, CookieService, FirestoreService, FireStorageService, NewsService],
	bootstrap: [AppComponent]
})
export class AppModule { }
