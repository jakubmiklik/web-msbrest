import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthorizationService } from '../services/authorization.service';
import {debounce, Observable} from 'rxjs';
import {getAuth, onAuthStateChanged} from '@angular/fire/auth';

@Injectable({
	providedIn: 'root'
})

export class SecureInnerPagesGuard implements CanActivate {
	constructor(
		public authService: AuthorizationService,
		public router: Router
	) { }

	canActivate(
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

		return new Promise((resolve, reject) => {
			onAuthStateChanged(getAuth(), (user) => {
				if (user) {
					resolve(true);
				} else {
					this.router.navigate(['prihlaseni']);
					resolve(false);
				}
			})
		})

	}

}
