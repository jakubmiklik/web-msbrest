export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}

export interface Child {
  id: string;
  name: string;
  absence?: Absence[];
}

export interface Absence {
  dateFrom: number;
  dateTo: number;
  text: string;
  childName: string;
}

export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  children?: NavItem[];
}

export interface CheckedChildren {
  checked: boolean;
}

export interface Novelty {
  title: string;
  date: number | string;
  text: string;
  files: FileInfo[];
  documents: FileInfo[];
  images: FileInfo[];
}

export interface MonthPlan {
  title: string;
  text: string;
  month: number;
  year: number;
}

export interface FileInfo {
  type: string;
  src: string;
  name: string;
}
